<?php

class Animal {
    public $legs = 4;
    public $name;
    public $cold_blooded= "no";

    public function __construct($name) {
        echo "Name : " . $this->name = $name . "<br>";
    }

    public function get_Legs() {
       echo "Legs : " . $this->legs . "<br>";
    }

    public function get_Cold_blooded() {
       echo "Cold Blooded : " .  $this->cold_blooded . "<br>";
    }
}

?>