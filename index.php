<?php

require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal('Shaun');
$sheep->name;
$sheep->get_Legs();
$sheep->get_Cold_blooded();

echo "<br>";

$kodok = new Frog("buduk");
$kodok->name;
$kodok->get_legs();
$kodok->get_Cold_blooded();
$kodok->jump();

echo "<br>";

$sungokong = new Ape('Kera Sakti');
$sungokong->name;
$sungokong->legs();
$sungokong->get_Cold_blooded();
$sungokong->yell();

?>